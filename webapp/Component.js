sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
	return AppComponent.extend("testListReport.Component", {
		metadata: {
			"manifest": "json"
		}
	});
});