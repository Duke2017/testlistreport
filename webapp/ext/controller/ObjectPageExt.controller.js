sap.ui.controller("testListReport.ext.controller.ObjectPageExt", {
	onInit: function () {
		this.extensionAPI.attachPageDataLoaded(function (oEvent) {
			sap.m.MessageToast.show(oEvent.context.getProperty("CategoryID"));
		});
	},
	onClickActionCategoriesHeader1: function (oEvent) {
		sap.m.MessageToast.show("Hello World!");
	},
	onClickActionCategoriesHeader2: function (oEvent) {
			this.getOwnerComponent().getModel().callFunction("/toActiveStatus",{
			method: "GET",
			success: function() {
				var oModel = this.getOwnerComponent().getModel();
				var sPreviousStatus = oModel.getProperty("/Categories(1)/Status");
				if (sPreviousStatus === "New") {
					oModel.setProperty("/Categories(1)/Status", "Deleted");
				} else {
					oModel.setProperty("/Categories(1)/Status", "Active");
				}
			}.bind(this),
			error: function() {
				sap.m.MessageToast.show("Error");
			}
		});  
	}
});